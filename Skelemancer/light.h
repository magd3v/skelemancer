#ifndef _LIGHT_H
#define _LIGHT_H

class Light
{
public:
	float radius; // the radius of the light in tiles
	float r, g, b;

	Light(float r, float g, float b, float radius);
	void lightArea(int x, int y);
};

#endif