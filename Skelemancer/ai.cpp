#include "stdafx.h"
#include "main.h"
#include <math.h>

void MonsterAi::update(Actor *owner)
{
	if(owner->destructible && owner->destructible->isDead())
	{
		return;
	}

	if(engine.map->isInFov(owner->x, owner->y))
	{
		moveOrAttack(owner, engine.player->x, engine.player->y);
	}
}

TeamAi::TeamAi(int faction)
{
	this->playerMinion = false;
	this->faction = faction;
}

MinionAi::MinionAi()
{
	this->playerMinion = true;
}

void TeamAi::update(Actor *owner)
{
	if(owner->destructible && owner->destructible->isDead())
	{
		return;
	}
	std::vector<Actor *> nearby = engine.getNearbyActors(owner->x, owner->y, 4);
	for(int i = 0; i != nearby.size(); i++)
	{
		Actor *actor = nearby[i];

		//engine.gui->message(actor->col, "%s says hi to %s.", owner->name, actor->name);
		if(actor)
		{
			if(actor->ai && actor->ai->playerMinion)
			{
				moveOrAttack(owner, actor->x, actor->y);
				break;
			}
			if((actor->ai && actor->ai->faction != owner->ai->faction) || actor == engine.player)
			{
				moveOrAttack(owner, actor->x, actor->y);
				break;
			}
		}
		stayNear(owner, actor, 10);
	}
}

void MinionAi::update(Actor *owner)
{
	int followDist = 0;

	if(owner->destructible && owner->destructible->isDead())
	{
		return;
	}

	std::vector<Actor *> nearby = engine.getNearbyActors(owner->x, owner->y, 10);
	for(int i = 0; i != nearby.size(); i++)
	{
		Actor *actor = nearby[i];
		//engine.gui->message(actor->col, "%s says hi to %s.", owner->name, actor->name);

		if(actor && actor->destructible && !actor->destructible->isDead() && actor->ai)
		{
			if(actor != engine.player && !actor->ai->playerMinion)
			{
				moveOrAttack(owner, actor->x, actor->y);
				followDist = 8;
			}
		}
		else
		{
			followDist = 3;
		}
	}
	stayNear(owner, engine.player, followDist);
}

void Ai::moveOrAttack(Actor *owner, int targetx, int targety)
{
	int dx = targetx - owner->x;
	int dy = targety - owner->y;
	float distance = sqrtf(dx * dx + dy * dy);

	if(distance >= 2)
	{
		TCODPath *path = new TCODPath(engine.map->map);
		path->compute(owner->x, owner->y, targetx, targety);
		int nextPosx = owner->x;
		int nextPosy = owner->y;

		path->walk(&nextPosx, &nextPosy, false);

		if(engine.map->canWalk(nextPosx, nextPosy))
		{
			owner->x = nextPosx;
			owner->y = nextPosy;
		}
		delete path;
	}
	else if(owner->attacker)
	{
		std::vector<Actor*> actorsOnTile = engine.getActor(targetx, targety);
		for(int i = 0; i != actorsOnTile.size(); i++)
		{
			if(actorsOnTile[i]->destructible && !actorsOnTile[i]->destructible->isDead())
			{
				owner->attacker->attack(owner, actorsOnTile[i]);
				break;
			}
		}
	}
}

void Ai::stayNear(Actor *owner, Actor *follow, int maxDist)
{
	int dx = follow->x - owner->x;
	int dy = follow->y - owner->y;
	float distance = sqrtf(dx * dx + dy * dy);

	if(distance > maxDist)
	{
		TCODPath *path = new TCODPath(engine.map->map);
		path->compute(owner->x, owner->y, follow->x, follow->y);
		int nextPosx = owner->x;
		int nextPosy = owner->y;

		path->walk(&nextPosx, &nextPosy, false);

		if(engine.map->canWalk(nextPosx, nextPosy))
		{
			owner->x = nextPosx;
			owner->y = nextPosy;
		}
		delete path;
	}
}