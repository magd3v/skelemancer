#ifndef _CAMERA_H
#define _CAMERA_H

struct ViewBounds 
	{ 
		int startx;
		int endx; 
		int starty; 
		int endy; 
	};

class Camera
{
public:
	int x, y;
	int viewWidth, viewHeight;

	Camera(int x, int y, int viewWidth, int viewHeight);
	void update();
	
	bool isInView(int x, int y) const;
	ViewBounds viewBounds;
	ViewBounds getViewBounds();
};

#endif