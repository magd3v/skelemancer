#ifndef _EFFECTOR_H
#define _EFFECTOR_H

struct Effect
{
public:
	Effect(int type, int duration);
	int type;
	int duration;
};

class Effector
{
public:
	Effector();
	~Effector();

	TCODList<Effect *> effects;
	enum effectTypes
	{
		Blazing,
		Poisoned,
		Butthurt
	}effectTypes;

	void addEffect(Actor *owner, int effect, int duration);
	void updateEffects(Actor *owner);
};

#endif