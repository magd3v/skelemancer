#ifndef _SKILLBOOK_H
#define _SKILLBOOK_H

class Skill
{
public:
	virtual bool use(Skill *skill) = 0;

	char *name;
	int type;
	int cost;
	int level;
};

class Skillbook
{
public:
	Skillbook(int maxSkills, int skillPoints);
	~Skillbook();

	TCODList<Skill*> skills;
	int maxSkills;
	int skillPoints;

	void addSkill(Skill *skill);
	void removeSkill(Skill *skill);
	void upgradeSkill(Skill *skill);
};

class RangedSpell : public Skill
{
	/*
	Ranged spells are cast at a specified position
	with an optional range for AOEs
	*/

public:
	int damage;
	int range;
	int radius;
	int x, y;
};

class DirectionalSpell : public Skill
{
	/*
	Directional spells start from the actor's position and 
	go in a straight line in the specified direction
	*/

public:
	int damage;
	int range;
	int direction;
	int x, y;
};

class Firebolt : public RangedSpell
{
public:
	Firebolt(int level);
	bool use(Skill *skill);
};

class SummonSkeleton : public RangedSpell
{
public:
	SummonSkeleton(int level);
	bool use(Skill *skill);
};

#endif