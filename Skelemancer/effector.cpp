#include "stdafx.h"
#include "main.h"

Effector::Effector()
{
}

Effect::Effect(int type, int duration) : type(type), duration(duration)
{
}

Effector::~Effector()
{
	effects.clearAndDelete();
}

void Effector::addEffect(Actor *owner, int type, int duration)
{
	Effect *effect = new Effect(type, duration);
	effects.push(effect);
}

void Effector::updateEffects(Actor *owner)
{
	if(!owner->destructible->isDead())
	{
		for(Effect **it = effects.begin(); it < effects.end(); it++)
		{
			Effect *effect = *it;
			if(effect->duration > 0)
			{
				switch(effect->type)
				{
				case(Effector::Butthurt):
					engine.gui->message(TCODColor::purple, "%s is butthurt!", owner->name);
					owner->destructible->takeDamage(owner, 4);
					break;
				case(Effector::Blazing):
					if(owner != engine.player)
						engine.gui->message(TCODColor::orange, "%s is burning!", owner->name);
					else
						engine.gui->message(TCODColor::orange, "you're burning!");

					owner->destructible->takeDamage(owner, 4);
					break;
				}
				effect->duration--;
			}
			else
			{
				effects.removeFast(it);
			}
		}
	}
}