#include "stdafx.h"
#include "main.h"
#include <vector>

void Engine::init(bool keepStatus)
{
	player = new Actor(40, 25, 192, "Magnus", TCODColor::white);
	player->destructible = new PlayerDestructible(32, 2, "your corpse");
	player->attacker = new Attacker(5);
	player->playercontroller = new PlayerController();
	player->container = new Container(26);
	player->light = new Light(0.6, 0.6, 0.6, engine.fovRadius / 2);
	player->effector = new Effector();
	player->skillbook = new Skillbook(5, 1);
	actors.push(player);
}

Engine::Engine(int screenWidth, int screenHeight) : gameStatus(STARTUP), fovRadius(20), screenWidth(screenWidth), screenHeight(screenHeight)
{
	TCODConsole::setCustomFont("terminal2.png", TCOD_FONT_LAYOUT_TCOD | TCOD_FONT_TYPE_GREYSCALE, 32, 8);
	TCODConsole::initRoot(screenWidth, screenHeight, "Skelemancer", false);

	TCODConsole::mapAsciiCodeToFont(192, 0, 5);
	TCODConsole::mapAsciiCodeToFont(193, 1, 5);
	TCODConsole::mapAsciiCodeToFont(194, 2, 5);
	TCODConsole::mapAsciiCodeToFont(195, 3, 5);
	TCODConsole::mapAsciiCodeToFont(196, 4, 5);
	TCODConsole::mapAsciiCodeToFont(197, 0, 6);
	TCODConsole::mapAsciiCodeToFont(198, 0, 7);

	camera = new Camera(5, 5, screenWidth, screenHeight);
	gui = new Gui();

	seeEverything = false;
	TCODConsole::setKeyboardRepeat(500, 40);

	gui->message(TCODColor::green, "Summoned to Obelisk");
	TCODSystem::setRenderer(TCOD_RENDERER_SDL);

	floor = 0;

	init(false);
	map = new Map(50, 50);
}

Engine::~Engine()
{
	delete graphics;
	actors.clearAndDelete();
	delete map;
	delete gui;
}

void Engine::sendToBack(Actor *actor)
{
	actors.remove(actor);
	actors.insertBefore(actor, 0);
}

std::vector<Actor*> Engine::getActor(int x, int y)
{
	std::vector<Actor*> finalVector;

	for(Actor **iterator = actors.begin(); iterator != actors.end(); iterator++)
	{
		Actor *actor = *iterator;

		if(actor->x == x && actor->y == y)
		{
			finalVector.push_back(*iterator);
		}
	}

	return finalVector;
}

void Engine::update()
{
	if(gameStatus == STARTUP) 
	{
		map->computeFov(); 
		camera->update(); 
		map->calculateLightmap();
		engine.render();
		Skill *firebolt = new Firebolt(2);
		player->skillbook->addSkill(firebolt);
		Skill *summonskel = new SummonSkeleton(3);
		player->skillbook->addSkill(summonskel);
		Skill *summonskel2 = new SummonSkeleton(4);
		player->skillbook->addSkill(summonskel2);
		Skill *summonskel3 = new SummonSkeleton(5);
		player->skillbook->addSkill(summonskel3);
		Skill *summonskel4 = new SummonSkeleton(6);
		player->skillbook->addSkill(summonskel4);

		player->playercontroller->displayHelp();

		//player->effector->addEffect(player, Effector::Butthurt, 8);
		//player->effector->addEffect(player, Effector::Blazing, 8);

		gameStatus = IDLE;
	}

	TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);
	player->update();

	if(gameStatus == NEW_TURN)
	{
		camera->update();

		for(Actor **iterator=actors.begin(); iterator != actors.end(); iterator++)
		{
			Actor *actor = *iterator;
			if(actor != player)
			{
				actor->update();
			}
			if(actor->effector)
			{
				actor->effector->updateEffects(actor);
			}
		}

		map->calculateLightmap();
	}
	gameStatus = IDLE;
}

void Engine::render()
{
	TCODConsole::root->clear();
	map->render();

	for(Actor **iterator = actors.begin(); iterator != actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor != engine.player && map->isInFov(actor->x, actor->y) && actor->visible || seeEverything)
		{
			actor->render();
		}
	}
	player->render();
	gui->render();
}

Actor *Engine::getClosestMonster(int x, int y, float range) const
{
	Actor *closest = NULL;
	float bestDistance = 1E6f;

	for(Actor **iterator = actors.begin(); iterator != actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->destructible && !actor->destructible->isDead())
		{
			float distance = actor->getDistance(x, y);
			if(distance < bestDistance && (distance <= range || range == 0.0f) && distance > 1.0f)
			{
				bestDistance = distance;
				closest = actor;
			}
		}
	}
	return closest;
}

std::vector<Actor*> Engine::getNearbyActors(int x, int y, float range) const
{
	float bestDistance = 1E6f;
	std::vector<Actor*> nearby;

	for(Actor **iterator = actors.begin(); iterator != actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->destructible && !actor->destructible->isDead())
		{
			float distance = actor->getDistance(x, y);
			if(distance < bestDistance && (distance <= range || range == 0.0f) && distance > 0.0f)
			{
				bestDistance = distance;
				nearby.push_back(actor);
			}
		}
	}
	return nearby;
}

float Engine::getDistance(int ax, int bx, int ay, int by)
{
	int dx = ax - bx;
	int dy = ay - by;
	return sqrtf(dx * dx + dy * dy);
}

void Engine::waitForSpecificKey(int keycode, TCOD_key_t &key)
{
	//wait for a keypress
	for(int i = 0; i != 1;)
	{
		TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL, true);
		if(key.vk == keycode || TCODConsole::isWindowClosed())
		{
			i = 1;
		}
		else
		{
			i = 0;
		}
	}
}

bool Engine::pickATile(int *x, int *y, float maxRange, float radius)
{
	while(!TCODConsole::isWindowClosed())
	{
		ViewBounds bounds = engine.camera->getViewBounds();
		render();

		int relPosx = mouse.cx + engine.camera->x;
		int relPosy = mouse.cy + engine.camera->y;

		TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS|TCOD_EVENT_MOUSE, &lastKey, &mouse);

		//highlight the possible range
		for(int cx = bounds.startx; cx < bounds.endx; cx++)
		{
			for(int cy = bounds.starty; cy < bounds.endy; cy++)
			{
				int relx = cx - engine.camera->x;
				int rely = cy - engine.camera->y;

				if(map->isInFov(cx, cy) && (maxRange == 0 || player->getDistance(cx, cy) <= maxRange))
				{
					TCODColor col = TCODConsole::root->getCharBackground(relx, rely);
					col.r += + 20;
					col.g += + 20;
					col.b += + 20;

					TCODConsole::root->setCharBackground(relx, rely, col);
				}

				if( map->isInFov(relPosx, relPosy) && ( maxRange == 0 || player->getDistance(relPosx, relPosy) <= maxRange ))
				{
					if(getDistance(mouse.cx, relx, mouse.cy, rely) <= radius)
					{
						TCODColor col = TCODConsole::root->getCharBackground(relx, rely);
						col.r += + 50;

						TCODConsole::root->setCharBackground(relx, rely, col);
					}
				}
			}
		}

		if( map->isInFov(relPosx, relPosy) && ( maxRange == 0 || player->getDistance(relPosx, relPosy) <= maxRange ))
		{
			TCODConsole::root->setCharBackground(mouse.cx, mouse.cy, TCODColor::white);
			if(mouse.lbutton_pressed)
			{
				*x = relPosx;
				*y = relPosy;
				return true;
			}
		}

		if(mouse.rbutton_pressed || lastKey.vk != TCODK_NONE)
		{
			return false;
		}
		TCODConsole::flush();
	}
	return false;
}