#ifndef _ENGINE_H
#define _ENGINE_H

#include <vector>

class Engine
{
public:
	bool seeEverything;
	int floor;

	TCODList<Actor *> actors;
	Actor *player;
	Map *map;
	Gui *gui;
	Camera *camera;

	int fovRadius;
	int screenWidth;
	int screenHeight;
	TCOD_key_t lastKey;
	TCOD_mouse_t mouse;
	TCODImage *graphics;

	void waitForSpecificKey(int keycode, TCOD_key_t &key);
	void printGameWindow(int width, int height);
	bool pickATile(int *x, int *y, float maxRange = 0.0f, float maxRadius = 1);
	std::vector<Actor*> getActor(int x, int y);
	std::vector<Actor*> Engine::getNearbyActors(int x, int y, float range) const;

	Engine(int screenWidth, int screenHeight);
	~Engine();
	void update();
	void render();
	void sendToBack(Actor *actor);
	void init(bool keepStatus);

	Actor *getClosestMonster(int x, int y, float range) const;
	float getDistance(int ax, int bx, int ay, int by);

	enum GameStatus
	{
		STARTUP, IDLE, NEW_TURN, VICTORY, DEFEAT
	}gameStatus;

private:
	bool computeFov;
};

extern Engine engine;

#endif