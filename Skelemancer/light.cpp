#include "stdafx.h"
#include "main.h"
#include <math.h>

Light::Light(float r, float g, float b, float radius) : r(r), g(g), b(b), radius(radius)
{
	//engine.numOfLights++;
	//printf("\n%i", engine.numOfLights);
}


void Light::lightArea(int lx, int ly)
{
	ViewBounds bounds = engine.camera->getViewBounds();

	int minx = lx - radius;
	int miny = ly - radius;
	int maxx = lx + radius;
	int maxy = ly + radius;

	minx = MAX(bounds.startx, 0);
	miny = MAX(bounds.starty, 0);
	maxx = MIN(bounds.endx, engine.map->getWidth() - 1);
	maxy = MIN(bounds.endy, engine.map->getWidth() - 1);

	TCODMap lmap(maxx - minx + 1, maxy - miny + 1);

	for(int x = minx; x < maxx; x++)
	{
		for(int y = miny; y < maxy; y++)
		{
			lmap.setProperties(x - minx, y - miny, engine.map->map->isWalkable(x, y), engine.map->map->isTransparent(x, y));
		}
	}

	lmap.computeFov(lx - minx, ly - miny, radius);

	for(int x = minx; x < minx + lmap.getWidth(); x++)
	{
		for(int y = miny; y < miny + lmap.getHeight(); y++)
		{
			if(engine.map->isInFov(x, y))
			{
				LightmapCell prevLight = engine.map->getLight(x, y);

				int squaredDist = (lx-x)*(lx-x)+(ly-y)*(ly-y);

				float intensityCoef1 = 1.0-squaredDist/(radius*radius);
				float intensityCoef2 = intensityCoef1 - 1.0/(1.0+radius*radius);
				float intensityCoef3 = intensityCoef2 / (1.0 - 1.0/(1.0+radius*radius));

				float nr = (intensityCoef3 * r) +  prevLight.r;
				float ng = (intensityCoef3 * g) + prevLight.g;
				float nb = (intensityCoef3 * b + ((0.006 / (radius / 2)) * squaredDist)) + prevLight.b;

				if(intensityCoef3 > 0)
				{
					if(lmap.isInFov(x - minx, y - miny))
					{
						engine.map->setLight(x, y, nr, ng, nb);
					}
				}
			}
		}
	}
}