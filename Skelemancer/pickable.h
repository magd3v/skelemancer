#ifndef _PICKABLE_H
#define _PICKABLE_H

class Pickable
{
public:
	bool pick(Actor *owner, Actor *wearer);
	void drop(Actor *owner, Actor *wearer);
	virtual bool use(Actor *owner, Actor *wearer);
};

class Healer : public Pickable
{
public:
	float amount; //how much HP it heals

	Healer(float amount);
	bool use(Actor *owner, Actor *wearer);
};

class LightningScroll : public Pickable
{
public:
	float range, damage;
	LightningScroll(float range, float damage);
	bool use(Actor *owner, Actor *wearer);
};

class Fireball : public LightningScroll
{
public:
	Fireball(float range, float damage);
};

#endif