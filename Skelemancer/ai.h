#ifndef _AI_H
#define _AI_H

class Ai
{
public:
	void moveOrAttack(Actor *owner, int targetx, int targety);
	void stayNear(Actor *owner, Actor *follow, int maxDist);
	virtual void update(Actor *owner) = 0;
	int faction;
	bool playerMinion;
};

class MonsterAi : public Ai
{
public:
	void update(Actor *owner);
};

class TeamAi : public Ai
{
public:
	TeamAi(int faction);
	void update(Actor *owner);
};

class MinionAi : public Ai
{
public:
	MinionAi();
	void update(Actor *owner);
};

#endif