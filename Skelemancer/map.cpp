#include "stdafx.h"
#include "main.h"

static const int ROOM_MAX_SIZE = 12;
static const int ROOM_MIN_SIZE = 8;
static const int DUNGEON_TURBULENCE = 8;
static const int MAX_ROOM_MONSTERS = 1;
static const int MAX_ROOM_ITEMS = 1;
static const float AMBIENT_LIGHT = 0.3;

class BspListener : public ITCODBspCallback
{
private:
	Map &map;
	int roomNum;
	int lastx, lasty;

public:
	BspListener(Map &map) : map(map), roomNum(0) {}
	
	bool visitNode(TCODBsp *node, void *userData)
	{
		if(node->isLeaf())
		{
			int x, y, w, h;

			//dig a room
			w = map.rng->getInt(ROOM_MIN_SIZE, node->w - 2);
			h = map.rng->getInt(ROOM_MIN_SIZE, node->h - 2);
			x = map.rng->getInt(node->x + 1, node->x + node->w - w - 1);
			y = map.rng->getInt(node->y + 1, node->y + node->h - h - 1);

			map.createRoom(roomNum == 0, x, y, x + w - 1, y + h - 1);

			if(roomNum != 0)
			{
				//dig a corridor from the last room
				map.dig(lastx, lasty, x + w / 2, lasty);
				map.dig(x + w / 2, lasty, x + w / 2, y + h / 2);
			}
			
			if(roomNum == 3)
			{
				Actor *orc = new Actor(lastx, lasty, 198, "Stairs up", TCODColor::white);
				orc->light = new Light(0.0, 0.4, 0.4, 20);
				orc->blocks = false;
				orc->stairs = true;
				engine.actors.push(orc);
			}

			Actor *orc = new Actor(lastx, lasty, 0, "", TCODColor::white);
			orc->light = new Light(map.rng->getFloat(0.0, 0.1), map.rng->getFloat(0.2, 0.55), map.rng->getFloat(0.2, 0.5), 10);
			orc->blocks = false;
			orc->visible = false;
			engine.actors.push(orc);

			lastx = x + w / 2;
            lasty = y + h / 2;
            roomNum++;
		}
		return true;
	}
};

void Map::generateMap(int width, int height)
{
	engine.actors.clear();

	tiles = new Tile[width * height];
	lightmap = new LightmapCell[width * height];
	map = new TCODMap(width, height);

	seed = TCODRandom::getInstance()->getInt(0, 0x7FFFFFFF);
	rng = new TCODRandom(seed, TCOD_RNG_CMWC);

	TCODBsp bsp(0, 0, width, height);
	bsp.splitRecursive(NULL, DUNGEON_TURBULENCE, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
	BspListener listener(*this);
	
	bsp.traverseInvertedLevelOrder(&listener, NULL);
}

Map::Map(int width, int height) : width(width), height(height)
{
	seed = TCODRandom::getInstance()->getInt(0, 0x7FFFFFFF);
	rng = new TCODRandom(seed, TCOD_RNG_CMWC);

	tiles = new Tile[width * height];
	lightmap = new LightmapCell[width * height];
	map = new TCODMap(width, height);

	TCODBsp bsp(0, 0, width, height);
	bsp.splitRecursive(NULL, DUNGEON_TURBULENCE, ROOM_MAX_SIZE, ROOM_MAX_SIZE, 1.5f, 1.5f);
	BspListener listener(*this);
	
	bsp.traverseInvertedLevelOrder(&listener, NULL);
}


Map::~Map()
{
	delete [] tiles;
	delete lightmap;
	delete map;
}

Tile Map::getTile(int x, int y) const
{
	return tiles[x + y * width];
}

bool Map::isWall(int x, int y) const
{
	return !map->isWalkable(x, y);
}

void Map::setColor(int x, int y, TCODColor color) const
{
	tiles[x + y * width].color = color;
}

int Map::getWidth()
{
	return width;
}

int Map::getHeight()
{
	return height;
}

void Map::dig(int x1, int y1, int x2, int y2)
{
	if(x2 < x1)
	{
		int tmp = x2;
		x2 = x1;
		x1 = tmp;
	}

	if(y2 < y1)
	{
		int tmp = y2;
		y2 = y1;
		y1 = tmp;
	}

	for (int tilex = x1; tilex <= x2; tilex++) 
	{
        for (int tiley = y1; tiley <= y2; tiley++) 
		{
			map->setProperties(tilex, tiley, true, true);
        }
    }
}

void Map::addMonster(int x, int y)
{
	TCODRandom *rng = TCODRandom::getInstance();
	if(rng->getInt(0, 2) == 0)
	{
		Actor *jelk = new Actor(x, y, 194, "Jelk", TCODColor::white);
		jelk->destructible = new MonsterDestructible(15, 1, "Jelk corpse");
		jelk->attacker = new Attacker(3);
		jelk->ai = new TeamAi(1);
		engine.actors.push(jelk);
	}
	if(rng->getInt(0, 2) == 1)
	{
		Actor *orc = new Actor(x, y, 195, "Rabla", TCODColor::white);
		orc->destructible = new MonsterDestructible(24, 2, "Hella ded Rabla");
		orc->attacker = new Attacker(4);
		orc->ai = new TeamAi(1);
		orc->effector = new Effector();
		engine.actors.push(orc);
	}
	if(rng->getInt(0, 2) == 2)
	{
		Actor *orc = new Actor(x, y, 196, "Gabber Jabber", TCODColor::white);
		orc->destructible = new MonsterDestructible(24, 2, "Gabber jabber corpse");
		orc->attacker = new Attacker(3);
		orc->ai = new TeamAi(1);
		orc->effector = new Effector();
		engine.actors.push(orc);
	}
}

void Map::createRoom(bool first, int x1, int y1, int x2, int y2)
{
	dig(x1, y1, x2, y2);
	if(first)
	{
		//put the player in the middle of the first room
		engine.player->x = (x1 + x2) / 2;
		engine.player->y = (y1 + y2) / 2;
	}
	else
	{
		TCODRandom *rng = TCODRandom::getInstance();
		int nbMonsters = rng->getInt(0, MAX_ROOM_MONSTERS);

		while(nbMonsters > 0)
		{
			int x = rng->getInt(x1, x2);
			int y = rng->getInt(y1, y2);
			if(canWalk(x, y))
			{
				addMonster(x, y);
			}
			nbMonsters--;
		}

		int nbItems = rng->getInt(0, MAX_ROOM_ITEMS);
		while(nbItems > 0)
		{
			int x = rng->getInt(x1, x2);
			int y = rng->getInt(y1, y2);

			if(canWalk(x, y))
			{
				addItem(x, y);
			}
			nbItems--;
		}
	}

	setTileColors();
}

bool Map::canWalk(int x, int y) const
{
	if(isWall(x, y))
	{
		return false;
	}
	for (Actor **iterator=engine.actors.begin(); iterator!=engine.actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->blocks && actor->x == x && actor->y == y)
		{
			//there is an actor there, cannot walk
			return false;
		}
	}
	return true;
}

void Map::setTileColors()
{
	TCODRandom *rng = TCODRandom::getInstance();

	for(int x = 0; x < width; x++)
	{
		for(int y = 0; y < height; y++)
		{
			if(!isWall(x, y))
			{
				int rand = rng->getInt(30, 31);
				setColor(x, y, TCODColor(rand, rand, rand));
			}
			else
			{
				int rand = rng->getInt(40, 48);
				setColor(x, y, TCODColor(rand, 48, rand));
			}
		}
	}
}

LightmapCell Map::getLight(int x, int y)
{
	return lightmap[x + y * width];
}

void Map::setLight(int x, int y, float r, float g, float b)
{
	lightmap[x + y * width].r = r;
	lightmap[x + y * width].g = g;
	lightmap[x + y * width].b = b;
}

bool Map::isInFov(int x, int y) const
{
	if(engine.seeEverything)
	{
		return true;
	}
	if(x < 0 || x >= width || y < 0 || y >= height)
	{
		return false;
	}
	if(map->isInFov(x, y))
	{
		tiles[x + y * width].explored = true;
		return true;
	}
	return false;
}

bool Map::isExplored(int x, int y) const
{
	return tiles[x + y * width].explored;
}

void Map::computeFov()
{
	map->computeFov(engine.player->x, engine.player->y, engine.fovRadius);
}

void Map::calculateLightmap()
{
	ViewBounds bounds = engine.camera->getViewBounds();

	for(int x = bounds.startx; x < bounds.endx; x++)
	{
		for(int y = bounds.starty; y < bounds.endy; y++)
		{
			setLight(x, y, AMBIENT_LIGHT, AMBIENT_LIGHT, AMBIENT_LIGHT);
		}
	}

	//Fast, popping method
	/*
	for (Actor **iterator=engine.actors.begin(); iterator!=engine.actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->light && isInFov(actor->x, actor->y))
		{
			actor->light->lightArea(actor->x, actor->y);
		}
	}*/
	//Slow, smooth method
	
	/*
	for(int x = bounds.startx; x < bounds.endx; x++)
	{
		for(int y = bounds.starty; y < bounds.endy; y++)
		{
			std::vector<Actor*> actorsOnTile = engine.getActor(x, y);

			for(int i = 0; i < actorsOnTile.size(); i++)
			{
				Actor *currentActor = actorsOnTile[i];

				if(currentActor->light)
				{
					currentActor->light->lightArea(currentActor->x, currentActor->y);
				}
			}
		}
	}*/
	//Faster, some popping

	for(int x = 0; x < engine.map->width; x++)
	{
		for(int y = 0; y < engine.map->height; y++)
		{
			std::vector<Actor*> actorsOnTile = engine.getActor(x, y);

			for(int i = 0; i < actorsOnTile.size(); i++)
			{
				Actor *currentActor = actorsOnTile[i];

				if(currentActor->light)
				{
					currentActor->light->lightArea(currentActor->x, currentActor->y);
				}
			}
		}
	}
	
}

void Map::render() const
{
	TCODColor tileColor;

	ViewBounds bounds = engine.camera->getViewBounds();

	for(int x = bounds.startx; x < bounds.endx; x++)
	{
		for(int y = bounds.starty; y < bounds.endy; y++)
		{
			tileColor = tiles[x + y * width].color;
			LightmapCell light = engine.map->getLight(x, y);
			int finalr = (tileColor.r * light.r);
			int finalg = (tileColor.g * light.g);
			int finalb = (tileColor.b * light.b); 

			if(finalr > 255)
				finalr = 255;
			if(finalg > 255)
				finalg = 255;
			if(finalb > 255)
				finalb = 255;

			TCODColor finalColor(finalr, finalg, finalb);

			int relativePosx = x - engine.camera->x;
			int relativePosy = y - engine.camera->y;

			if (isInFov(x, y)) 
			{
				TCODConsole::root->setCharBackground(relativePosx, relativePosy, finalColor);
			} 
			else if (isExplored(x,y)) 
			{
				TCODConsole::root->setCharBackground(relativePosx, relativePosy, tileColor * AMBIENT_LIGHT);
			}
		}
	}
}

void Map::addItem(int x, int y)
{
	TCODRandom *rng = TCODRandom::getInstance();
	int dice = rng->getInt(0, 2);

	if(dice == 0 || dice == 1)
	{
		Actor *healthPotion = new Actor(x, y, 197, "Health potion", TCODColor(200, 0, 200));
		healthPotion->blocks = false;
		healthPotion->pickable = new Healer(6);
		healthPotion->light = new Light(0.3, 0.0, 0.3, 6);
		engine.actors.push(healthPotion);
		engine.sendToBack(healthPotion);
	}
	else if (dice == 2)
	{
		Actor *scrollOfLightningBolt = new Actor(x, y, '#', "Soaw Xoloea", TCODColor::lightGreen);
		scrollOfLightningBolt->blocks = false;
		scrollOfLightningBolt->pickable = new LightningScroll(5, 22);
		scrollOfLightningBolt->light = new Light(0.3, 0.3, 0.0, 6);
		engine.actors.push(scrollOfLightningBolt);
		engine.sendToBack(scrollOfLightningBolt);
	}
	else if (dice == 3)
	{
		Actor *mush = new Actor(x, y, 12, "Glowshroom", TCODColor(245, 255, 255));
		mush->blocks = false;
		mush->light = new Light(0.3, 0.0, 0.3, 6);
		engine.actors.push(mush);
		engine.sendToBack(mush);
	}
}