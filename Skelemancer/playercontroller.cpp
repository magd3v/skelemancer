#include "stdafx.h"
#include "main.h"

void PlayerController::update(Actor *owner)
{
	if(owner->destructible && owner->destructible->isDead())
	{
		//if killable, if dead, do nothing
		return;
	}

	int dx = 0, dy = 0;
	switch(engine.lastKey.vk)
	{
	case TCODK_UP : dy = -1; break;
    case TCODK_DOWN : dy = 1; break;
    case TCODK_LEFT : dx = -1; break;
    case TCODK_RIGHT : dx = 1; break;

	case TCODK_KP8 : dy = -1; break;
    case TCODK_KP2 : dy = 1; break;
    case TCODK_KP4 : dx = -1; break;
    case TCODK_KP6 : dx = 1; break;

	case TCODK_KP7 : dy = -1; dx = -1; break;
    case TCODK_KP1 : dy = 1; dx = -1; break;
    case TCODK_KP3 : dy = 1; dx = 1; break;
    case TCODK_KP9 : dy = -1; dx = 1; break;

	case TCODK_CHAR: handleActionKey(owner, engine.lastKey.c); break;
    default:break;
	}

	if(dx != 0 || dy != 0)
	{
		engine.gameStatus = engine.NEW_TURN;
		if(moveOrAttack(owner, owner->x + dx, owner->y + dy))
		{
			engine.map->computeFov();
		}
	}
}

void PlayerController::displayHelp()
{
	static const int WINDOW_WIDTH = 54;
	static const int WINDOW_HEIGHT = 33;
	static const int SECTION1 = 2;
	static const int SECTION2 = 15;

	static TCODConsole con(WINDOW_WIDTH, WINDOW_HEIGHT);

	//display the window
	con.setDefaultBackground(TCODColor(160, 160, 160));
	con.setDefaultForeground(TCODColor(50, 50, 50));
	con.printFrame(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, true, TCOD_BKGND_DEFAULT, "HELP");

	//display help
	con.setDefaultForeground(TCODColor(120, 120, 225));
	con.print(2, SECTION1,		"############");
	con.print(2, SECTION1 + 1,	"#          #");
	con.print(2, SECTION1 + 2,	"############");

	con.setDefaultForeground(TCODColor(120, 225, 120));
	con.print(2, SECTION1 + 4, "[%c%c%c%c/NUMPAD]", 24, 25, 26, 27);
	con.print(2, SECTION1 + 6, "   [?]");
	con.print(2, SECTION1 + 7, "   [g]");
	con.print(2, SECTION1 + 8, "   [k]");
	con.print(2, SECTION1 + 9, "   [i]");
	con.print(2, SECTION1 + 10, "[NUM5]");

	con.setDefaultForeground(TCODColor(150, 150, 150));
	con.print(16, SECTION1 + 4, "Movement");

	con.print(9, SECTION1 + 6, "Help");
	con.print(9, SECTION1 + 7, "Get item");
	con.print(9, SECTION1 + 8, "Skillbook");
	con.print(9, SECTION1 + 9, "Inventory");
	con.print(9, SECTION1 + 10, "Wait");

	con.setDefaultForeground(TCODColor(120, 120, 225));
	con.print(2, SECTION2,		"############");
	con.print(2, SECTION2 + 1,	"#          #");
	con.print(2, SECTION2 + 2,	"############");

	con.setDefaultForeground(TCODColor(180, 180, 235));
	con.print(4, SECTION1 + 1, "CONTROLS");
	con.print(6, SECTION2 + 1, "INFO");

	con.setDefaultForeground(TCODColor(180, 80, 180));
	con.print(2, SECTION2 + 4,	"Start by summoning skeletons. You can blast your \nown skeletons with spells to charge them with \nelemental power.");

	con.setDefaultForeground(TCODColor(235, 130, 60));
	con.print(2, SECTION2 + 8, "Use souls to purchase upgrades for your skeletons \nand skills.");

	con.setDefaultForeground(TCODColor(100, 205, 205));
	con.print(2, SECTION2 + 11, "Each skeleton summon requires a pile of bones as a \ncatalyst. Don't forget to stock up from corpses!");

	con.setDefaultForeground(TCODColor(100, 100, 100));
	con.print(2, SECTION2 + 15, "Press ESC to close this window.");
	
	//blit the inventory console on the root console
	TCODConsole::blit(&con, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, TCODConsole::root, engine.screenWidth / 2 - WINDOW_WIDTH/2, engine.screenHeight / 2 - WINDOW_HEIGHT / 2);
	TCODConsole::flush();

	TCOD_key_t key;
	engine.waitForSpecificKey(TCODK_ESCAPE, key);
}

Actor *PlayerController::choseFromInventory(Actor *owner)
{
	static const int INVENTORY_WIDTH = 50;
	static const int INVENTORY_HEIGHT = 28;
	static TCODConsole con(INVENTORY_WIDTH, INVENTORY_HEIGHT);

	//display the inventory frame
	con.setDefaultBackground(TCODColor(50, 80, 200));
	con.printFrame(0, 0, INVENTORY_WIDTH, INVENTORY_HEIGHT, true, TCOD_BKGND_DEFAULT, "Inventory");

	//display the items with their keyboard shortcut
	con.setDefaultBackground(TCODColor::white);
	int shortcut = 'a';
	int y = 1;
	for(Actor **it = owner->container->inventory.begin(); it != owner->container->inventory.end(); it++)
	{
		Actor *actor = *it;
		con.print(2, y, "(%c) %s", shortcut, actor->name);
		y++;
		shortcut++;
	}

	//blit the inventory console on the root console
	TCODConsole::blit(&con, 0, 0, INVENTORY_WIDTH, INVENTORY_HEIGHT, TCODConsole::root, engine.screenWidth / 2 - INVENTORY_WIDTH/2, engine.screenHeight / 2 - INVENTORY_HEIGHT / 2);
	TCODConsole::flush();

	//wait for a keypress
	TCOD_key_t key;
	TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL, true);

	if(key.vk == TCODK_CHAR)
	{
		int actorIndex = key.c - 'a';
		if(actorIndex >= 0 && actorIndex < owner->container->inventory.size())
		{
			return owner->container->inventory.get(actorIndex);
		}
	}
	return NULL;
}

Skill *PlayerController::choseFromSkillbook(Actor *owner)
{
	static const int SKILLBOOK_WIDTH = 20;
	static const int SKILLBOOK_HEIGHT = 30;
	static TCODConsole con(SKILLBOOK_WIDTH, SKILLBOOK_HEIGHT);

	//display the skillbook frame
	con.setDefaultBackground(TCODColor(50, 80, 200));
	con.printFrame(0, 0, SKILLBOOK_WIDTH, SKILLBOOK_HEIGHT, true, TCOD_BKGND_DEFAULT, "Skillbook");

	//display the skills
	con.setDefaultBackground(TCODColor::white);

	int shortcut = 'a';
	int y = 1;

	for(Skill **it = owner->skillbook->skills.begin(); it != owner->skillbook->skills.end(); it++)
	{
		Skill *skill = *it;
		con.print(2, y, "(%c) %s%i", shortcut, skill->name, skill->level);
		y++;
		shortcut++;
	}

	//blit
	TCODConsole::blit(&con, 0, 0, SKILLBOOK_WIDTH, SKILLBOOK_HEIGHT, TCODConsole::root, engine.screenWidth / 2 - SKILLBOOK_WIDTH/2, engine.screenHeight / 2 - SKILLBOOK_HEIGHT / 2);
	TCODConsole::flush();

	//wait for a keypress
	TCOD_key_t key;
	TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, NULL, true);

	if(key.vk == TCODK_CHAR)
	{
		int skillIndex = key.c - 'a';
		if(skillIndex >= 0 && skillIndex < owner->skillbook->skills.size())
		{
			return owner->skillbook->skills.get(skillIndex);
		}
	}
	return NULL;
}

bool PlayerController::moveOrAttack(Actor *owner, int targetx, int targety)
{
	if(engine.map->isWall(targetx, targety))
	{
		return false;
	}

	//look for living actors to attack
	for(Actor **iterator = engine.actors.begin(); iterator != engine.actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->destructible && !actor->destructible->isDead() && actor->x == targetx && actor->y == targety)
		{
			owner->attacker->attack(owner, actor);
			return false;
		}
	}

	//move to cell
	owner->x = targetx;
	owner->y = targety;
	return true;
}

void PlayerController::handleActionKey(Actor *owner, int ascii)
{
	if(ascii == 'g') //pick up item
	{
		bool found = false;
		for (Actor **iterator = engine.actors.begin(); iterator < engine.actors.end(); iterator++)
		{
			Actor *actor = *iterator;
			if(actor->pickable && actor->x == owner->x && actor->y == owner->y)
			{
				if(actor->pickable->pick(actor, owner))
				{
					found = true;
					engine.gui->message(TCODColor::lightGreen, "You pick up %s", actor->name);
					break;
				}
				else if(!found)
				{
					found = true;
					engine.gui->message(TCODColor::lightRed, "Your inventory is full!");
				}
			}
		}
		if(!found)
		{
			engine.gui->message(TCODColor::lightGrey, "There's nothing there to pick up, you dingus!");
		}
		engine.gameStatus = Engine::NEW_TURN;
	}
	else if(ascii == 'i') //display inventory
	{
		Actor *actor = choseFromInventory(owner);
		if(actor)
		{
			actor->pickable->use(actor, owner);
			engine.gameStatus = Engine::NEW_TURN;
		}
	}
	else if(ascii == 'u') //go up one level
	{
		std::vector<Actor*> actorsOnTile = engine.getActor(engine.player->x, engine.player->y);

			for(int i = 0; i < actorsOnTile.size(); i++)
			{
				Actor *currentActor = actorsOnTile[i];

				if(currentActor->stairs)
				{
					Actor *tempPlayer = engine.player;

					engine.map->generateMap(50, 50);
					engine.gameStatus = Engine::NEW_TURN;
					engine.map->computeFov();

					engine.player = tempPlayer;
				}
			}
	}
	else if(ascii == 'k') //display inventory
	{
		Skill *skill = choseFromSkillbook(owner);
		if(skill)
		{
			skill->use(skill);
			engine.gameStatus = Engine::NEW_TURN;
		}
	}
	else if(ascii == 'd') //drop item
	{
		Actor *actor = choseFromInventory(owner);
		if(actor)
		{
			actor->pickable->drop(actor, owner);
			engine.gameStatus = Engine::NEW_TURN;
		}
	}
	else if(ascii == '?') //display help
	{
		displayHelp();
	}
}