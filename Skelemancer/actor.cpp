#include "stdafx.h"
#include "main.h"
#include <math.h>

Actor::Actor(int x, int y, int ch, const char *name, const TCODColor &col) : 
	x(x), y(y), ch(ch), name(name), col(col), 
	blocks(true), visible(true), stairs(false), attacker(NULL), destructible(NULL), ai(NULL), playercontroller(NULL),
	pickable(NULL), container(NULL), light(NULL), effector(NULL), skillbook(NULL)
	{}


Actor::~Actor()
{
	if(attacker) delete attacker;
	if(destructible) delete destructible;
	if(ai) delete ai;
	if(playercontroller) delete playercontroller;
	if(pickable) delete pickable;
	if(container) delete container;
	if(light) delete light;
	if(effector) delete effector;
	if(skillbook) delete skillbook;
}

void Actor::render() const
{
	int relativePosx = this->x - engine.camera->x;
	int relativePosy = this->y - engine.camera->y;

	float highlightr = MAX(0, engine.map->getLight(this->x, this->y).r - 0.3);
	float highlightg = MAX(0, engine.map->getLight(this->x, this->y).g - 0.3);
	float highlightb = MAX(0, engine.map->getLight(this->x, this->y).b - 0.3);

	TCODColor finalColor = TCODColor(MIN((int)(col.r * highlightr), 255), MIN((int)(col.g * highlightg), 255), MIN((int)(col.b * highlightb), 255));

	if(engine.camera->isInView(this->x, this->y) || engine.seeEverything)
	{
		TCODConsole::root->setCharForeground(relativePosx, relativePosy, finalColor);
		TCODConsole::root->setChar(relativePosx, relativePosy, ch);
	}
}

bool Actor::moveOrAttack(int x, int y)
{
	if(engine.map->isWall(x, y))
	{
		return false;
	}

	for(Actor **iterator=engine.actors.begin(); iterator != engine.actors.end(); iterator++)
	{
		//scans through each actor, if one of their positions = the one you're moving to...
		Actor *actor = *iterator;
		if(actor->x == x && actor->y == y)
		{
			printf("The %s laughs at your puny efforts to attack him!\n", actor->name);
			return false;
		}
	}
	this->x = x;
	this->y = y;
	return true;
}

void Actor::update()
{
	if(ai) ai->update(this);
	if(playercontroller) playercontroller->update(this);
}

float Actor::getDistance(int cx, int cy) const 
{
	int dx = x - cx;
	int dy = y - cy;
	return sqrtf(dx * dx + dy * dy);
}