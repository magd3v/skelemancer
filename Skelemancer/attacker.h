#ifndef _ATTACKER_H
#define _ATTACKER_H

class Attacker
{
public:
	float power;

	Attacker(float power);
	void attack(Actor *owner, Actor *target);
};

#endif