// Skelemancer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "main.h"

Engine engine(60, 35);

int _tmain(int argc, _TCHAR* argv[])
{
	while(!TCODConsole::isWindowClosed())
	{
		engine.update();
		engine.render();
		TCODConsole::flush();
	}

	return 0;
}