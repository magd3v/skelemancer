#ifndef _PLAYERCONTROLLER_H
#define _PLAYERCONTROLLER_H

class PlayerController
{
public:
	void update(Actor *owner);
	void displayHelp();

protected:
	Actor *choseFromInventory(Actor *owner);
	Skill *choseFromSkillbook(Actor *owner);
	bool moveOrAttack(Actor *owner, int targetx, int targety);
	void handleActionKey(Actor *owner, int ascii);
};

#endif