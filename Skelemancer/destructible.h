#ifndef _DESTRUCTIBLE_H
#define _DESTRUCTIBLE_H

class Destructible
{
public:
	float maxHp;
	float hp;
	float defense;
	const char *corpseName;

	float heal(float amount);
	Destructible(float maxHP, float defense, const char *corpseName);
	inline bool isDead() {return hp <= 0;}
	float takeDamage(Actor *owner, float damage);
	virtual void die(Actor *owner);
};

class MonsterDestructible : public Destructible
{
public:
	MonsterDestructible(float maxHP, float defense, const char *corpseName);
	void die(Actor *owner);
};

class PlayerDestructible : public Destructible
{
public:
	PlayerDestructible(float maxHP, float defense, const char *corpseName);
	void die(Actor *owner);
};

#endif