struct Tile
{
	bool explored; //Has the player already seen the tile?
	TCODColor color;
	Tile() : explored(false){}
};

struct LightmapCell
{
	float r, g, b;
};

class Map
{
public:
	TCODMap *map;

	Map(int width, int height);
	~Map();

	Tile getTile(int x, int y) const;
	int getWidth();
	int getHeight();

	void addMonster(int x, int y);
	void addItem(int x, int y);
	void generateMap(int width, int height);

	bool canWalk(int x, int y) const;
	bool isWall(int x, int y) const;
	bool isInFov(int x, int y) const;
	bool isExplored(int x, int y) const;

	LightmapCell getLight(int x, int y);
	void setLight(int x, int y, float r, float g, float b);
	void lightArea(int lx, int ly, float radius, float r, float g, float b);
	void setColor(int x, int y, TCODColor color) const;

	void setTileColors();

	void computeFov();
	void render() const;
	void calculateLightmap();

private:
	int width, height;
	
protected:
	Tile *tiles;
	LightmapCell *lightmap;
	long seed;
	TCODRandom *rng;

	friend class BspListener;

	void dig(int x1, int y1, int x2, int y2);
	void createRoom(bool first, int x1, int y1, int x2, int y2);
};

