#include "stdafx.h"
#include "main.h"

Attacker::Attacker(float power) : power(power)
{
}

void Attacker::attack(Actor *owner, Actor *target)
{
	if(engine.map->isInFov(target->x, target->y))
	{
		if (target->destructible && ! target->destructible->isDead())
		{
			if (power - target->destructible->defense > 0)
			{
				if(owner == engine.player)
				{
					engine.gui->message(TCODColor::white, "You punch %s.", target->name);
				}
				else if(target == engine.player)
				{
					engine.gui->message(TCODColor::white, "%s attacks you.", owner->name);
				}
				else
				{
					engine.gui->message(TCODColor::white, "%s attacks %s.", owner->name, target->name);
				}
			}

			float damage = target->destructible->takeDamage(target, power);

			if (damage == 0)
			{
				engine.gui->message(TCODColor::lightGrey, "%s attacks %s, but it has no effect!", owner->name, target->name);
			}
		}
		else
		{
			engine.gui->message(TCODColor::lightGrey, "%s attacks %s in vain.",owner->name,target->name);
		}
	}
}