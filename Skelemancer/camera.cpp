#include "stdafx.h"
#include "main.h"

Camera::Camera(int x, int y, int viewWidth, int viewHeight) : x(x), y(y), viewWidth(viewWidth), viewHeight(viewHeight)
{
}

void Camera::update()
{
	x = (engine.player->x - viewWidth / 2);
	y = (engine.player->y - viewHeight / 2) + 5;
}

bool Camera::isInView(int x, int y) const
{
	if(x > this->x && y > this->y && x < this->x + viewWidth && y < this->y + viewWidth + 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ViewBounds Camera::getViewBounds()
{
	int startx = engine.camera->x;
	int starty = engine.camera->y;
	int endx = engine.camera->x + engine.camera->viewWidth + 1;
	int endy = engine.camera->y + engine.camera->viewHeight + 1;

	if(startx < 0) {startx = 0;}
	if(starty < 0) {starty = 0;}
	if(endx > engine.map->getWidth()) {endx = engine.map->getWidth();}
	if(endy > engine.map->getHeight()) {endy = engine.map->getHeight();}

	ViewBounds bounds;
	bounds.startx = startx;
	bounds.starty = starty;
	bounds.endx = endx;
	bounds.endy = endy;

	return bounds;
}