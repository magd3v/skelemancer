#include "stdafx.h"
#include "main.h"

bool Pickable::pick(Actor *owner, Actor *wearer)
{
	if(wearer->container && wearer->container->add(owner))
	{
		engine.actors.remove(owner);
		return true;
	}
	return false;
}

void Pickable::drop(Actor *owner, Actor *wearer)
{
	if(wearer->container)
	{
		wearer->container->remove(owner);
		engine.actors.push(owner);
		owner->x = wearer->x;
		owner->y = wearer->y;
		engine.sendToBack(owner);
		engine.gui->message(TCODColor::lightGrey,"%s drops a %s.", wearer->name,owner->name);
	}
}

bool Pickable::use(Actor *owner, Actor *wearer)
{
	if(wearer->container)
	{
		wearer->container->remove(owner);
		delete owner;
		return true;
	}
	return false;
}

Healer::Healer(float amount) : amount(amount){}

bool Healer::use(Actor *owner, Actor *wearer)
{
	if(wearer->destructible)
	{
		float amountHealed = wearer->destructible->heal((int)(wearer->destructible->maxHp / 5));

		if(amountHealed > 0)
		{
			engine.gui->message(TCODColor(100, 100, 255), "Healed %i HP", (int)amountHealed);
			return Pickable::use(owner, wearer);
		}
	}
	engine.gui->message(TCODColor(255, 100, 100), "You already have full HP.");
	return false;
}

LightningScroll::LightningScroll(float range, float damage) : range(range), damage(damage){}

bool LightningScroll::use(Actor *owner, Actor *wearer)
{
	engine.gui->message(TCODColor(120, 120, 255), "You recieve +5 buh!");
	return Pickable::use(owner, wearer);
}

