#ifndef _ACTOR_H
#define _ACTOR_H

class Actor
{
public:
	int x, y; //position on the map
	int fovRadius; //Field of view radius
	int ch; //ascii code
	TCODColor col; //color
	const char *name; //the actor's name
	bool blocks; //can we walk on this actor?
	bool visible; //can we see this actor?
	bool stairs; //is this actor stairs to a higher level?
	Attacker *attacker; //something that deals damage
	Destructible *destructible; //something that can be damaged
	Ai *ai; //something self-updating
	PlayerController *playercontroller; //something that can be controlled by the player
	Pickable *pickable; //something that can be picked up and used
	Container *container; //something that can contain actors
	Light *light; //something that emits light
	Effector *effector; //something that inflcts status effects on the actor
	Skillbook *skillbook; //something that contains skills that the actor can use

	float getDistance(int cx, int cy) const;

	Actor(int x, int y, int ch, const char *name, const TCODColor &col);
	~Actor();
	void update();
	void render() const;
	bool moveOrAttack(int x, int y);
};

#endif