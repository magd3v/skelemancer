#include "stdafx.h"
#include "main.h"

Skillbook::Skillbook(int maxSkills, int skillPoints) : maxSkills(maxSkills), skillPoints(skillPoints)
{

}

Skillbook::~Skillbook()
{
	skills.clearAndDelete();
}

void Skillbook::addSkill(Skill *skill)
{
	if(skills.size() < maxSkills && !skills.contains(skill))
	{
		skills.push(skill);
		engine.gui->message(TCODColor::pink, "You learned %s%i!", skill->name, skill->level);
	}
	else
	{
		engine.gui->message(TCODColor::red, "Cannot learn %s%i!", skill->name, skill->level);
	}
}

void Skillbook::removeSkill(Skill *skill)
{
	if(skills.contains(skill))
	{
		skills.remove(skill);
		engine.gui->message(TCODColor::grey, "You forgot %s%i.", skill->name, skill->level);
	}
	else
	{
		engine.gui->message(TCODColor::red, "You cannot forget %s%i", skill->name, skill->level);
	}
}

void Skillbook::upgradeSkill(Skill *skill)
{
	if(skills.contains(skill) && skillPoints > 0) //add skillpoint check here
	{
		skill->level++;
		engine.gui->message(TCODColor::lightGreen, "You upgraded %s%i to %s%i!", skill->name, skill->level - 1, skill->name, skill->level);
	}
	else
	{
		engine.gui->message(TCODColor::red, "You cannot upgrade %s%i", skill->name, skill->level);
	}
}

Firebolt::Firebolt(int level)
{
	this->level = level;
	this->damage = level * 2;
	this->name = "firebolt";
	this->range = level + 3;
	this->radius = 2;
}

SummonSkeleton::SummonSkeleton(int level)
{
	this->level = level;
	this->damage = level * level + 1;
	this->name = "skeleton";
	this->range = 5;
	this->radius = 0;
}

bool Firebolt::use(Skill *skill)
{
	engine.gui->message(TCODColor::cyan, "Left-click a target tile for Firebolt, right-click to cancel.");
	int x, y;
	if(!engine.pickATile(&x, &y, range, radius))
	{
		return false;
	}

	//burn everything in range (including player)
	engine.gui->message(TCODColor::orange, "You conjure a bolt of fire!");
	for (Actor **iterator=engine.actors.begin(); iterator != engine.actors.end(); iterator++)
	{
		Actor *actor = *iterator;
		if(actor->destructible && !actor->destructible->isDead() && actor->getDistance(x,y) <= radius)
		{
			if(actor != engine.player)
			engine.gui->message(TCODColor::orange,"%s gets burned.", actor->name, damage);
			else
			engine.gui->message(TCODColor::orange,"you get burned.", damage);

			actor->destructible->takeDamage(actor, damage);
			if(actor->effector)
			{
				actor->effector->addEffect(actor, Effector::Blazing, this->level * 3);
			}
		}
	}
	return true;
}

bool SummonSkeleton::use(Skill *skill)
{
	engine.gui->message(TCODColor::cyan, "Left-click a target tile for Skeleton, right-click to cancel.");
	int x, y;
	if(!engine.pickATile(&x, &y, range, radius))
	{
		return false;
	}

	//Create a new skeleton
	engine.gui->message(TCODColor::orange, "You summon a skeleton!");
	Actor *skeleton = new Actor(x, y, 193, "Your skeleton", TCODColor::white);
	skeleton->destructible = new MonsterDestructible(level * 5, level / 5, "Pile O' Bones");
	skeleton->attacker = new Attacker(damage);
	skeleton->ai = new MinionAi();
	skeleton->effector = new Effector();
	skeleton->light = new Light(0.3, 0.3, 0.3, 4);
	engine.actors.push(skeleton);

	return true;
}